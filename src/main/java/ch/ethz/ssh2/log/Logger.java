/*
 * Copyright (c) 2006-2011 Christian Plattner. All rights reserved.
 * Please refer to the LICENSE.txt for licensing details.
 */
package ch.ethz.ssh2.log;

/**
 * Logger delegating to JRE logging.
 *
 * @author Christian Plattner
 * @version $Id$
 */

public class Logger {

	public static LogWriter writer;
	
    private static final int logLevel = 99;

    private String className;

    public final static Logger getLogger(@SuppressWarnings("rawtypes") Class x) {
        return new Logger(x);
    }

    public Logger(@SuppressWarnings("rawtypes") Class x) {
        this.className = x.getName();
    }

    public final boolean isEnabled() {
    	return null != writer && writer.isEnabled();
    }
    
    @SuppressWarnings("unused")
	public final void log(int level, String message) {
        if (null != writer)
        	writer.log(level, message);
    }
    
    public interface LogWriter {
    	void log(int level, String message);
    	boolean isEnabled();
    }
}
