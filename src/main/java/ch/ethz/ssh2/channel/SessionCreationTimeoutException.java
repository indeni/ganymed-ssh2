package ch.ethz.ssh2.channel;

public class SessionCreationTimeoutException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -764579048514179360L;

	public SessionCreationTimeoutException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SessionCreationTimeoutException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SessionCreationTimeoutException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public SessionCreationTimeoutException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
