package ch.ethz.ssh2.channel;

import ch.ethz.ssh2.Connection;

/**
 * Thrown when a session failed to open because there was some issue
 * with the server (and not with some third party or the client - which is us).
 *
 */
public class SessionCreationRefusedDueToServerIssueException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6513332237690655051L;

	final Connection assocConnection;
	
	public SessionCreationRefusedDueToServerIssueException(Connection assocConnection) {
		super();
		this.assocConnection = assocConnection;
	}

	public SessionCreationRefusedDueToServerIssueException(Connection assocConnection, String message, Throwable cause) {
		super(message, cause);
		this.assocConnection = assocConnection;
	}

	public SessionCreationRefusedDueToServerIssueException(Connection assocConnection, String message) {
		super(message);
		this.assocConnection = assocConnection;
	}

	public SessionCreationRefusedDueToServerIssueException(Connection assocConnection, Throwable cause) {
		super(cause);
		this.assocConnection = assocConnection;
	}

	public Connection getAssociatedConnection() {
		return assocConnection;
	}
	
}
