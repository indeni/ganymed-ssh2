/*
 * Copyright (c) 2006-2011 Christian Plattner. All rights reserved.
 * Please refer to the LICENSE.txt for licensing details.
 */

package ch.ethz.ssh2.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

import ch.ethz.ssh2.log.Logger;

/**
 * TimeoutService (beta). Here you can register a timeout.
 * <p>
 * Implemented having large scale programs in mind: if you open many concurrent SSH connections that rely on timeouts, then there will be only one
 * timeout thread. Once all timeouts have expired/are cancelled, the thread will (sooner or later) exit. Only after new timeouts arrive a new thread
 * (singleton) will be instantiated.
 *
 * MODIFIED BY INDENI: This class has had issues dealing with concurrency. Try to use it with a 60 second timeout for many handlers together (adding
 * and removing them at random) and you'll notice it reaches a situation where the main thread locks the todolist for a long time disallowing anyone
 * else to enter. So, we've modified the code to use ConcurrentlyLinkedQueue and no locking.
 * 
 * @author Christian Plattner
 * @version $Id$
 */
public class TimeoutService {

    private static final Logger log = Logger.getLogger(TimeoutService.class);

    public static class TimeoutToken implements Comparable {

        private long runTime;
        private Runnable handler;

        private TimeoutToken(long runTime, Runnable handler) {
            this.runTime = runTime;
            this.handler = handler;
        }

        public int compareTo(Object o) {
            TimeoutToken t = (TimeoutToken) o;
            if (runTime > t.runTime) return 1;
            if (runTime == t.runTime) return 0;
            return -1;
        }
    }

    private static class TimeoutThread extends Thread {
		@Override
		public void run()
		{
            if (log.isEnabled()) log.log(20, "In TimeoutService.TimeoutThread...");

            while (true)
            {
                if (todolist.size() == 0) {
                    timeoutThread = null;
                    return;
                }

                long now = System.currentTimeMillis();

                // Iterate over the list, find the closest token
                // (the first to expire)
                long earliestTimeFound = Long.MAX_VALUE;
                Iterator<TimeoutToken> itr = todolist.iterator();
                TimeoutToken tt = null;
                while (itr.hasNext()) {
                    TimeoutToken current = itr.next();
                    if (current.runTime < earliestTimeFound) {
                        earliestTimeFound = current.runTime;
                        tt = current;
                    }
                }

                // If someone managed to clear the list before we started iterating
                // over it, we should quit.
                if (null == tt) return;

                if (tt.runTime > now) {
                    /* Not ready yet, sleep a little bit */
                    try {
                        // Fairly busy loop, but this avoid all kinds of race
                        // conditions we've been experiencing.
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }

                    /*
                    * We cannot simply go on, since it could be that the token was removed (cancelled) or another one has been inserted in the meantime.
                    */

                    continue;
                }

                todolist.remove();

                try {
                    tt.handler.run();
                } catch (Exception e) {
                    StringWriter sw = new StringWriter();
                    e.printStackTrace(new PrintWriter(sw));
                    log.log(20, "Exeception in Timeout handler:" + e.getMessage() + "(" + sw.toString() + ")");
                }
            }
        }
    }

	/* The list object is also used for locking purposes */
    private static final ConcurrentLinkedQueue<TimeoutToken> todolist = new ConcurrentLinkedQueue<TimeoutToken>();

    private static Thread timeoutThread = null;
	private static Object timeoutThreadMonitor = new Object();

	/**
	 * It is assumed that the passed handler will not execute for a long time.
	 * 
	 * @param runTime
	 * @param handler
	 * @return a TimeoutToken that can be used to cancel the timeout.
	 */
	public static final TimeoutToken addTimeoutHandler(long runTime, Runnable handler)
	{
        TimeoutToken token = new TimeoutToken(runTime, handler);

        todolist.add(token);

		synchronized (timeoutThreadMonitor) {
            if (timeoutThread != null)
                timeoutThread.interrupt();
            else {
                timeoutThread = new TimeoutThread();
                timeoutThread.setDaemon(true);
                timeoutThread.start();
            }
        }

        return token;
    }

    public static final void cancelTimeoutHandler(TimeoutToken token) {
        todolist.remove(token);
    }
}
