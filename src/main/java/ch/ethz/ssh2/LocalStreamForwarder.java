/*
 * Copyright (c) 2006-2011 Christian Plattner. All rights reserved.
 * Please refer to the LICENSE.txt for licensing details.
 */
package ch.ethz.ssh2;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;

import ch.ethz.ssh2.channel.Channel;
import ch.ethz.ssh2.channel.ChannelManager;
import ch.ethz.ssh2.channel.LocalAcceptThread;
import ch.ethz.ssh2.channel.SessionCreationRefusedDueToServerIssueException;
import ch.ethz.ssh2.channel.SessionCreationTimeoutException;
import ch.ethz.ssh2.channel.Channel.ChannelCloseReason;
import ch.ethz.ssh2.channel.Channel.ChannelCloseReasonType;

/**
 * A <code>LocalStreamForwarder</code> forwards an Input- and Outputstream
 * pair via the secure tunnel to another host (which may or may not be identical
 * to the remote SSH-2 server).
 *
 * @author Christian Plattner
 * @version 2.50, 03/15/10
 */
public class LocalStreamForwarder
{
	private ChannelManager cm;

    private String host_to_connect;
    private int port_to_connect;
    private LocalAcceptThread lat;

	private Channel cn;

	LocalStreamForwarder(ChannelManager cm, String host_to_connect, int port_to_connect) throws IOException
	{
        this.cm = cm;
        this.host_to_connect = host_to_connect;
        this.port_to_connect = port_to_connect;

		cn = cm.initDirectTCPIPChannel(host_to_connect, port_to_connect,
				InetAddress.getLocalHost().getHostAddress(), 0);
	}

	/**
	 * @return An <code>InputStream</code> object.
	 * @throws IOException
     * @throws SessionCreationRefusedDueToServerIssueException 
	 */
    public InputStream getInputStream() throws IOException, SessionCreationRefusedDueToServerIssueException {
        try {
			cm.openDirectTCPIPChannel(cn, 0, host_to_connect, port_to_connect, "127.0.0.1", 0);
		} catch (SessionCreationTimeoutException e) {
			// Can't happen, no timeout in use
		}
		return cn.getStdoutStream();
	}

	/**
	 * Get the OutputStream. Please be aware that the implementation MAY use an
	 * internal buffer. To make sure that the buffered data is sent over the
	 * tunnel, you have to call the <code>flush</code> method of the
	 * <code>OutputStream</code>. To signal EOF, please use the
	 * <code>close</code> method of the <code>OutputStream</code>.
	 *
	 * @return An <code>OutputStream</code> object.
	 * @throws IOException
     * @throws SessionCreationRefusedDueToServerIssueException 
	 */
    public OutputStream getOutputStream() throws IOException, SessionCreationRefusedDueToServerIssueException {
        try {
			cm.openDirectTCPIPChannel(cn, 0, host_to_connect, port_to_connect, "127.0.0.1", 0);
		} catch (SessionCreationTimeoutException e) {
			// Can't happen, no timeout in use
		}
		return cn.getStdinStream();
	}

	/**
	 * Close the underlying SSH forwarding channel and free up resources.
	 * You can also use this method to force the shutdown of the underlying
	 * forwarding channel. Pending output (OutputStream not flushed) will NOT
	 * be sent. Pending input (InputStream) can still be read. If the shutdown
	 * operation is already in progress (initiated from either side), then this
	 * call is a no-op.
	 *
     * @param closeTimeout In order to close, a response must be received from the other side.
     * This is the timeout for waiting for a response. If timeout is reached, an exception is thrown. 
	 * @throws IOException
	 */
    public void close(long closeTimeout) throws IOException {
        cm.closeChannel(cn, new ChannelCloseReason(ChannelCloseReasonType.ClosedDueToUserRequest, ""), true, closeTimeout);
    }
    
    public Channel getChannel() {
    	return cn;
	}
}
