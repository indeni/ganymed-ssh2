//package com.indeni.ssh2client.loadtests;
//import java.util.concurrent.locks.*;
//import ch.ethz.ssh2.channel.*;
//import ch.ethz.ssh2.*;
//import java.io.*;
//
//public class Test250 {
//
//    private String host, user;
//    private File pem;
//    private int executed;
//    private ReentrantReadWriteLock lock;
//
//    private Test250(String h) {
//        host = h;
//        user = System.getProperty("user.name");
//        pem = new File(System.getProperty("user.home") + File.separator + ".ssh" + File.separator + "id_rsa");
//        lock = new ReentrantReadWriteLock();
//    }
//
//    private void run() throws IOException, InterruptedException {
//        lock.writeLock().lock();
//        try {
//            Thread threads[] = new Thread[250];
//            for (int i = threads.length - 1; i >= 0; i--) {
//                Thread thread = new ConnectionThread();
//                thread.start();
//                threads[i] = thread;
//            }
//        }
//        finally {
//            lock.writeLock().unlock();
//        }
//    }
//
//    public static void main(String args[]) {
//        if (args.length != 1) {
//            System.err.println("Host parameter is missing");
//            System.exit(1);
//        }
//        try {
//            new Test250(args[0]).run();
//            Thread.sleep(60*60*1000);
//        }
//        catch (IOException ioe) {
//            ioe.printStackTrace();
//            System.exit(1);
//        }
//        catch (InterruptedException ie) {
//            ie.printStackTrace();
//            System.exit(1);
//        }
//        System.exit(0);
//    }
//
//    private class ConnectionThread extends Thread {
//        private Connection connection;
//
//        private ConnectionThread() throws IOException {
//            connection = new Connection(host);
//            connection.connect();
//            connection.authenticateWithPublicKey(user, pem, null);
//            System.out.println("success");
//        }
//
//        public void run() {
//            try {
//                lock.readLock().lock();
//                for (int i = 0; i < 150; i++) {
//                    System.out.println(executed);
//                    Session session1 = null, session2 = null, session3 = null;
//                    try {
//                        session1 = connection.openSession();
//                        session1.execCommand("echo");
//                        session2 = connection.openSession();
//                        session2.execCommand("date");
//                        session3 = connection.openSession();
//                        session3.execCommand("find / -xdev");
//                        waitSession(session1);
//                        waitSession(session2);
//                        waitSession(session3);
//                    }
//                    finally {
//                        if (session1 != null)
//                            session1.close();
//                        if (session2 != null)
//                            session2.close();
//                        if (session3 != null)
//                            session3.close();
//                    }
//                    executed++;
//                }
//            }
//            catch (IOException ioe) {
//                ioe.printStackTrace();
//            }
//            catch (SessionCreationTimeoutException scte) {
//                scte.printStackTrace();
//            }
//        }
//
//        private void waitSession(Session session) throws IOException {
//            InputStream out = session.getStdout();
//            InputStream err = session.getStderr();
//            while (true) {
//                int condition = session.waitForCondition(ChannelCondition.EXIT_STATUS|ChannelCondition.STDERR_DATA|ChannelCondition.STDOUT_DATA, 0);
//                if ((condition&ChannelCondition.STDERR_DATA) != 0)
//                    err.skip(err.available());
//                if ((condition&ChannelCondition.STDOUT_DATA) != 0)
//                    out.skip(out.available());
//                if ((condition&ChannelCondition.EXIT_STATUS) != 0) {
//                    session.close();
//                    return;
//                }
//            }
//        }
//    }
//
//}
