//package com.indeni.ssh2client.loadtests;
//
//import java.io.IOException;
//
//import ch.ethz.ssh2.ChannelCondition;
//import ch.ethz.ssh2.Connection;
//import ch.ethz.ssh2.Session;
//import ch.ethz.ssh2.unittest.BaseUnitTest;
//import ch.ethz.ssh2.util.Host;
//
//
//public class ConnectionTest extends BaseUnitTest {
//
//    public static void main(String[] args) throws Exception {
//        Host host = new Host("localhost", 22, "shoaib", "shobi");
//        ConnectionTest unitTest = new ConnectionTest(host);
//
//        unitTest.testJustOpenCloseSession();
//
//        unitTest.testInvalidCommand();
//
//    }
//
//    private Host hostToConnect;
//
//    public ConnectionTest(Host host) {
//        this.hostToConnect = host;
//    }
//
//    public boolean testJustOpenCloseSession() throws IOException {
//        Connection conn = getConnection(hostToConnect);
//        if (!conn.isAuthenticationComplete()) return false;
//
//        Session s = conn.openSession();
//        s.close();
//        sleepThread(1000 * 3);
//
//        return true;
//    }
//
//    public boolean testInvalidCommand() throws Exception {
//        Connection conn = getConnection(hostToConnect);
//        if (!conn.isAuthenticationComplete()) return false;
//
//        Session s = conn.openSession();
//        try {
//            int a = 7 / 0; // just to skip execCommand
//            s.execCommand("echo debugging");
//        } catch (Exception e) {
//            // TODO: handle exception
//        }
//
//        s.waitForCondition(ChannelCondition.CLOSED | ChannelCondition.EOF | ChannelCondition.STDERR_DATA | ChannelCondition.STDOUT_DATA, 1000);
//        s.close();
//
//        sleepThread(1000);
//
//        Session s2 = conn.openSession();
//        s2.execCommand("echo debugging");
//
//        sleepThread(1000 * 3);
//
//        return true;
//    }
//
//
//}
